extends TileMap

class_name Board

var tile_by_name := {}
var layer_by_name := {}
var count: Dictionary = {}

@export var state_layer: String = "States"
@export var count_layer: String = "Count"
@export var board_layer: String = "Board"
@export var block_layer: String = "Block"
@export var state_name: String = "Name"
@export var empty_state: String = "Empty"
var off_limits: String = "OffLimits"
@export var active: bool = true
@export var tile_cooldown: int = 3
@export var reset_current_player = false


func _ready():
	print("Initializing board...")
	_index_atlas_coordinates_by_name()
	_index_layers_by_name()
	$Highlight.size = tile_set.tile_size
	print("Tiles: ", ", ".join(tile_by_name.keys()))

# Returns the state of the given grid per layer.
func get_state(grid_pos: Vector2) -> String:
	var state_tile_data: TileData = self.get_cell_tile_data(layer_by_name[state_layer], grid_pos)
	var board_tile_data: TileData = self.get_cell_tile_data(layer_by_name[board_layer], grid_pos)
	if not board_tile_data:
		return off_limits
	if not state_tile_data or not state_tile_data.get_custom_data(state_name):
		return empty_state
	return state_tile_data.get_custom_data(state_name)

# Calcuates the current symbol count of a given field
func add_count_to_tile(grid_pos: Vector2, state: String):
	# Adds a new key, value pair to the count dictionary if it doesnt exist yet. Each grid
	# has one dictionary entry consisting of the grid_coords as Key and a Vector2 as value
	# were x describes circles and y describes crosses.
	if !count.has(grid_pos):
		count[grid_pos] = {"Circle": 0, "Cross": 0, "Cooldown": 0}
	if count[grid_pos]["Cooldown"] == 0:
		reset_current_player = false
		if state == "Circle" && (count[grid_pos]["Circle"] - count[grid_pos]["Cross"]) < 3:
			count[grid_pos]["Circle"] = count[grid_pos]["Circle"] + 1;
			count[grid_pos]["Cooldown"] = tile_cooldown
		elif state == "Cross" && (count[grid_pos]["Cross"] - count[grid_pos]["Circle"]) < 3:
			count[grid_pos]["Cross"] = count[grid_pos]["Cross"] + 1;
			count[grid_pos]["Cooldown"] = tile_cooldown
		_decrease_cooldown(grid_pos)
		_display_tile_ownership(grid_pos)
	else:
		reset_current_player = true

#Changs the tiles ownership based on the number of symbols
func _display_tile_ownership(grid_pos: Vector2):
	#Red1 and Red2 are the names of the green and yellow tile, dont know how to change the custom data
	if count[grid_pos]["Circle"] > count[grid_pos]["Cross"]:
		self.set_cell(layer_by_name[state_layer], grid_pos, 1, tile_by_name["Red1"]["atlas_coords"])
		self.set_cell(layer_by_name[count_layer], grid_pos, 1, Vector2(0,(count[grid_pos]["Circle"] - count[grid_pos]["Cross"] + 1)))
	if count[grid_pos]["Cross"] > count[grid_pos]["Circle"]:
		self.set_cell(layer_by_name[state_layer], grid_pos, 1, tile_by_name["Red2"]["atlas_coords"])
		self.set_cell(layer_by_name[count_layer], grid_pos, 1, Vector2(3,(count[grid_pos]["Cross"] - count[grid_pos]["Circle"] + 1)))
	if count[grid_pos]["Cross"] == count[grid_pos]["Circle"]:
		self.set_cell(layer_by_name[state_layer],grid_pos, -1)
		self.set_cell(layer_by_name[count_layer], grid_pos, 1, Vector2(1,2))

func _decrease_cooldown(grid_pos: Vector2):
	for item in count:
		if count[item]["Cooldown"] != 0:
			self.set_cell(layer_by_name[block_layer], grid_pos, 1, tile_by_name["Blue2"]["atlas_coords"])			
			count[item]["Cooldown"] -= 1
			if count[item]["Cooldown"] == 0:
				self.set_cell(layer_by_name[block_layer], item, -1)			
			
func global_to_map(global_pos: Vector2) -> Vector2:
	return self.local_to_map(self.to_local(global_pos))

func gridpos_at_mouse() -> Vector2:
	return global_to_map(get_viewport().get_mouse_position()) 

func reset() -> void:
	for cell in get_used_cells(layer_by_name[state_layer]):
		set_cell(layer_by_name[state_layer], cell, -1)
	for cell in get_used_cells(layer_by_name[count_layer]):
		set_cell(layer_by_name[count_layer], cell, -1)
	for cell in get_used_cells(layer_by_name[block_layer]):
		set_cell(layer_by_name[block_layer], cell, -1)
	count = {}

func _index_atlas_coordinates_by_name():
	var tileset = self.tile_set
	for source_index in range(tileset.get_source_count()):
		var source_id = tileset.get_source_id(source_index)
		var atlas:TileSetAtlasSource = tileset.get_source(source_id)
		for tile_index in range(atlas.get_tiles_count()):
			var atlas_coords = atlas.get_tile_id(tile_index)
			var tile_data : TileData = atlas.get_tile_data(atlas_coords, 0)
			if tile_data and tile_data.get_custom_data(state_name):
				tile_by_name[tile_data.get_custom_data(state_name)] = {
					"source_id": source_id,
					"atlas_coords": atlas_coords
				}
	print(tile_by_name)
func _index_layers_by_name():
	for i in range(get_layers_count()):
		layer_by_name[get_layer_name(i)] = i
