extends Node2D

var current_player: int = 0
var player_states := ["Circle", "Cross"]
var won := ""

@onready var board := $Board as Board
@onready var nextPlayer := $GUI/NextPlayer as Sprite2D
@onready var wonPlayer := $GUI/WonPlayer as Sprite2D

# Called when the node enters the scene tree for the first time.
func _ready():
	current_player = 0
	board.reset()
	won = ""
	wonPlayer.hide()
	nextPlayer.region_rect.position = Vector2(0,100)
	nextPlayer.show()
	board.active = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if won == "":
		if Input.is_action_just_pressed("Click"):
			var grid_pos = board.gridpos_at_mouse()
			if board.get_state(grid_pos) != board.off_limits:
				board.add_count_to_tile(grid_pos,player_states[current_player])
				won = _check_win()
				if won=="" && board.reset_current_player == false:
					current_player = 1 - current_player
					if current_player == 1:
						nextPlayer.region_rect.position = Vector2(300,100)
					else:
						nextPlayer.region_rect.position = Vector2(0,100)
				elif won != "":
					print("Winner: ", won)
					if won == "Red1":
						wonPlayer.region_rect.position = Vector2(0,100)
					else:
						wonPlayer.region_rect.position = Vector2(300,100)
					wonPlayer.show()
					nextPlayer.hide()
					board.active = false

func _on_restart_button_pressed():
	_ready()


var _checks = [
	[Vector2i(-1,0), Vector2i(1,0)],
	[Vector2i(0,-1), Vector2i(0,1)],
	[Vector2i(-1,-1), Vector2i(1,1)],
	[Vector2i(-1,1), Vector2i(1,-1)],
]
				
func _check_win() -> String:
	for cell in board.get_used_cells(board.layer_by_name[board.state_layer]):
		var state = board.get_state(cell)
		for check in _checks:
			var same = true
			for neighbour in check:
				if board.get_state(cell + neighbour) != state:
					same = false
					continue
			if same:
				return state
				print(state)
	return ""
			


